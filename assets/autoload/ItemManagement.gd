extends Node

var holding_item = null;
var item_offset = Vector2(0, 0);

enum EquipmentSlotType {
	SLOT_DEFAULT = 0,
	SLOT_HELMET_LIGHT,
	SLOT_ARMOR_LIGHT,
	SLOT_GLOVES_LIGHT,
	SLOT_LEGS_LIGHT,
	SLOT_FEET_LIGHT,
	SLOT_HELMET_MEDIUM,
	SLOT_ARMOR_MEDIUM,
	SLOT_GLOVES_MEDIUM,
	SLOT_LEGS_MEDIUM,
	SLOT_FEET_MEDIUM,
	SLOT_HELMET_HEAVY,
	SLOT_ARMOR_HEAVY,
	SLOT_GLOVES_HEAVY,
	SLOT_LEGS_HEAVY,
	SLOT_FEET_HEAVY,
	SLOT_NECK,
	SLOT_RING,
	SLOT_RING2,
	SLOT_WEAPON,
	SLOT_SHIELD
}
enum ItemRarity {
	NORMAL = 0,
	MAGIC,
	RARE,
	LEGENDARY
}
const RarityColor = {
	ItemRarity.NORMAL: {
		"background": "#808080",
		"border": "#cccccc"
	},
	ItemRarity.MAGIC: {
		"background": "#1b51d1",
		"border": "#2000ff"
	},
	ItemRarity.RARE: {
		"background": "#ffdf1d",
		"border": "#CCB217"
	},
	ItemRarity.LEGENDARY: {
		"background": "#ec5300",
		"border": "#BC4200"
	}
}

var ItemNameToSlot = {
	"LightHelmet": EquipmentSlotType.SLOT_HELMET_LIGHT,
	"LightArmor": EquipmentSlotType.SLOT_ARMOR_LIGHT,
	"LightGloves": EquipmentSlotType.SLOT_GLOVES_LIGHT,
	"LightLegs": EquipmentSlotType.SLOT_LEGS_LIGHT,
	"LightBoots": EquipmentSlotType.SLOT_FEET_LIGHT,
	"MediumHelmet": EquipmentSlotType.SLOT_HELMET_MEDIUM,
	"MediumArmor": EquipmentSlotType.SLOT_ARMOR_MEDIUM,
	"MediumGloves": EquipmentSlotType.SLOT_GLOVES_MEDIUM,
	"MediumLegs": EquipmentSlotType.SLOT_LEGS_MEDIUM,
	"MediumBoots": EquipmentSlotType.SLOT_FEET_MEDIUM,
	"HeavyHelmet": EquipmentSlotType.SLOT_HELMET_HEAVY,
	"HeavyArmor": EquipmentSlotType.SLOT_ARMOR_HEAVY,
	"HeavyGloves": EquipmentSlotType.SLOT_GLOVES_HEAVY,
	"HeavyLegs": EquipmentSlotType.SLOT_LEGS_HEAVY,
	"HeavyBoots": EquipmentSlotType.SLOT_FEET_HEAVY,
	"Necklace": EquipmentSlotType.SLOT_NECK,
	"Ring": EquipmentSlotType.SLOT_RING,
	"Ring2": EquipmentSlotType.SLOT_RING2,
	"Sword": EquipmentSlotType.SLOT_WEAPON,
	"Shield": EquipmentSlotType.SLOT_SHIELD
}

#const ItemSlotClass = preload("res://items/ItemSlot.gd");
#const ItemClass = preload("res://items/item.gd");

# Called when the node enters the scene tree for the first time.
func _ready():
	#set_process_input(false)
	pass

func mouse_enter_slot(_slot):
	var player_tooltip = get_node("/root/TestLevel/MyPlayer/PlayerMenuPanels/Tooltip");
	if _slot.item:
		player_tooltip.display(_slot.item)
	else:
		if holding_item:
			if canEquip(holding_item, _slot):
				_slot.style.bg_color = Color("#FFFFFF");

func mouse_exit_slot(_slot):
	var player_tooltip = get_node("/root/TestLevel/MyPlayer/PlayerMenuPanels/Tooltip");
	if player_tooltip.visible:
		player_tooltip.hide();
	_slot.refreshColors()

func slot_gui_input(event : InputEvent, slot):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT && event.pressed:
			if holding_item:
				if slot.slotType != global.EquipmentSlotType.SLOT_DEFAULT:
					if canEquip(holding_item, slot):
						if !slot.item:
							slot.equipItem(holding_item, false);
							#set_process_input(false)
							holding_item = null;
						else:
							var tempItem = slot.item;
							slot.pickItem();
							tempItem.rect_global_position = event.global_position - item_offset;
							slot.equipItem(holding_item, false);
							set_process_input(true)
							holding_item = tempItem;
				elif slot.item:
					var tempItem = slot.item;
					slot.pickItem();
					tempItem.rect_global_position = event.global_position - item_offset;
					slot.putItem(holding_item);
					set_process_input(true)
					holding_item = tempItem;
				else:
					slot.putItem(holding_item);
					holding_item = null;
					#set_process_input(false)
			elif slot.item:
				holding_item = slot.item;
				set_process_input(true)
				item_offset = event.global_position - holding_item.rect_global_position;
				slot.pickItem();
				holding_item.rect_global_position = event.global_position - item_offset;
#		elif event.button_index == BUTTON_RIGHT && !event.pressed:
#			if slot.slotType != Global.EquipmentSlotType.SLOT_DEFAULT:
#				if slot.item:
#					var freeSlot = getFreeSlot();
#					if freeSlot:
#						var item = slot.item;
#						slot.removeItem();
#						freeSlot.setItem(item);
#			else:
#				if slot.item:
#					var itemSlotType = slot.item.slotType;
#					var panelSlot = characterPanel.getSlotByType(slot.item.slotType);
#					if itemSlotType == Global.EquipmentSlotType.SLOT_RING:
#						if panelSlot[0].item && panelSlot[1].item:
#							var panelItem = panelSlot[0].item;
#							panelSlot[0].removeItem();
#							var slotItem = slot.item;
#							slot.removeItem();
#							slot.setItem(panelItem);
#							panelSlot[0].setItem(slotItem);
#							pass
#						elif !panelSlot[0].item && panelSlot[1].item || !panelSlot[0].item && !panelSlot[1].item:
#							var tempItem = slot.item;
#							slot.removeItem();
#							panelSlot[0].equipItem(tempItem);
#						elif panelSlot[0].item && !panelSlot[1].item:
#							var tempItem = slot.item;
#							slot.removeItem();
#							panelSlot[1].equipItem(tempItem);
#							pass
#					else:
#						if panelSlot.item:
#							var panelItem = panelSlot.item;
#							panelSlot.removeItem();
#							var slotItem = slot.item;
#							slot.removeItem();
#							slot.setItem(panelItem);
#							panelSlot.setItem(slotItem);
#						else:
#							var tempItem = slot.item;
#							slot.removeItem();
#							panelSlot.equipItem(tempItem);
	pass
	
func _input(event : InputEvent):
	#print("input process de merde")
	if event is InputEventMouseMotion:
		if holding_item && holding_item.picked:
			holding_item.rect_global_position = event.global_position - item_offset;
	elif event is InputEventMouseButton:
		print(event)
	else:
		pass

func canEquip(item, slot):
	var ring = EquipmentSlotType.SLOT_RING;
	var ring2 = EquipmentSlotType.SLOT_RING2;
	return item.slotType == slot.slotType || item.slotType == ring && (slot.slotType == ring || slot.slotType == ring2) || slot.slotType == EquipmentSlotType.SLOT_DEFAULT;

