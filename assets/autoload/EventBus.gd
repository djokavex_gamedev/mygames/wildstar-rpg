extends Node

signal player_gain_xp(experience)
signal player_level_up()

signal enemy_died()

signal equipment_changed()
