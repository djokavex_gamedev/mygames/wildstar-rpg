extends KinematicBody2D

var floating_damage = preload("res://assets/others/FloatingText.tscn")

# General stats
export var base_level = 1 # Base level.
export var base_assault_power = 10.0 # Assault Power increases the damage of most Assault and Utility actions.
export var base_support_power = 10.0 # Support Power increases the healing and damage of most Support and Utility actions.
export var base_max_health = 1000.0 # Max Health is the total amount of damage the player can take before dying.
export var base_max_shield = 500.0 # Max Shields is the total amount of damage the player's shield can mitigate.
# Offensive stats
export var base_strikethrough = 10.0 # Reduces the chance foes have to Deflect your attacks. Strikethrough that exceeds a foe's Deflect rate is converted into Armor Pierce.
export var base_crit_hit_chance = 10.0 # Your chance to Critically Hit a foe or Critically Heal an ally.
export var base_crit_hit_severity = 25.0 # Critical damage and healing increased to this percent.
export var base_multi_hit_chance = 10.0 # Your chance for each application of damage or healing to be applied an additional time at a reduced value.
export var base_multi_hit_severity = 25.0 # Percent of damage or healing granted from the Multi-Hit.
export var base_vigor = 10.0 # Your damage is increased up to this percent, based on your current health.
export var base_armor_pierce = 10.0 # Your attacks ignore this percent of the foe's mitigation.
# Defensive stats
export var base_physical_mitigation = 10.0 # Physical damage reduction.
export var base_technology_mitigation = 10.0 # Technology damage reduction.
export var base_magic_mitigation = 10.0 # Magic damage reduction.
export var base_glance_mitigation = 10.0 # Damage reduction triggered by a Glance.
export var base_glance_chance = 10.0 # Your chance to reduce incoming damaging attack by your Glance Mitigation.
export var base_critical_mitigation = 10.0 # Reduces bonus damage caused by a foe's Critical Hit.
export var base_deflect_chance = 10.0 # Your chance to Deflect an attack, avoiding all damage from that attack.
export var base_deflect_crit_hit_chance = 10.0 # Reduces the chance foes have to Critically Hit you.
# Shield stats
export var base_shield_mitigation = 75.0 # The percentage of damage redirected to your Shield.
export var base_regen_rate = 1.0 # The percentage of your Max Shield that will be regenerated each Shield Tick Time.
export var base_reboot_time = 5.0 # The time before your Shield will begin to regenerate after taking damage.
export var base_tick_time = 10.0 # The frequency at which your Shield regenerates.
# Utility stats
export var base_cooldown_reduction = 10.0 # All class ability cooldowns are reduced by this percent.
export var base_cc_duration = 10.0 # All applied CC durations are modified by this percent.
export var base_lifesteal = 10.0 # Your damage heals you for this percent of damage done.
export var base_focus_pool = 2000.0 # Total amount of focus.
export var base_focus_recovery_rate = 1.5 # Percent of your Focus Pool that you regenerate each second while in combat.
export var base_focus_cost_reduction = 10.0 # The focus costs of your actions are reduced by this percentage.
export var base_reflect_chance = 10.0 # Your chance to return damage dealt to you back to the foe at a reduced value.
export var base_reflect_damage = 10.0 # Percent of damage that is dealt to a foe by a Reflect.
export var base_intensity = 10.0 # Your outgoing healing amount and focus costs are increased by this percent.

# General stats
var current_level = 1 # Base level.
var current_assault_power = 10.0 # Assault Power increases the damage of most Assault and Utility actions.
var current_support_power = 10.0 # Support Power increases the healing and damage of most Support and Utility actions.
var current_max_health = 1000.0 # Max Health is the total amount of damage the player can take before dying.
var current_max_shield = 500.0 # Max Shields is the total amount of damage the player's shield can mitigate.
# Offensive stats
var current_strikethrough = 10.0 # Reduces the chance foes have to Deflect your attacks. Strikethrough that exceeds a foe's Deflect rate is converted into Armor Pierce.
var current_crit_hit_chance = 10.0 # Your chance to Critically Hit a foe or Critically Heal an ally.
var current_crit_hit_severity = 25.0 # Critical damage and healing increased to this percent.
var current_multi_hit_chance = 10.0 # Your chance for each application of damage or healing to be applied an additional time at a reduced value.
var current_multi_hit_severity = 25.0 # Percent of damage or healing granted from the Multi-Hit.
var current_vigor = 10.0 # Your damage is increased up to this percent, based on your current health.
var current_armor_pierce = 10.0 # Your attacks ignore this percent of the foe's mitigation.
# Defensive stats
var current_physical_mitigation = 10.0 # Physical damage reduction.
var current_technology_mitigation = 10.0 # Technology damage reduction.
var current_magic_mitigation = 10.0 # Magic damage reduction.
var current_glance_mitigation = 10.0 # Damage reduction triggered by a Glance.
var current_glance_chance = 10.0 # Your chance to reduce incoming damaging attack by your Glance Mitigation.
var current_critical_mitigation = 10.0 # Reduces bonus damage caused by a foe's Critical Hit.
var current_deflect_chance = 10.0 # Your chance to Deflect an attack, avoiding all damage from that attack.
var current_deflect_crit_hit_chance = 10.0 # Reduces the chance foes have to Critically Hit you.
# Shield stats
var current_shield_mitigation = 75.0 # The percentage of damage redirected to your Shield.
var current_regen_rate = 1.0 # The percentage of your Max Shield that will be regenerated each Shield Tick Time.
var current_reboot_time = 5.0 # The time before your Shield will begin to regenerate after taking damage.
var current_tick_time = 10.0 # The frequency at which your Shield regenerates.
# Utility stats
var current_cooldown_reduction = 10.0 # All class ability cooldowns are reduced by this percent.
var current_cc_duration = 10.0 # All applied CC durations are modified by this percent.
var current_lifesteal = 10.0 # Your damage heals you for this percent of damage done.
var current_focus_pool = 2000.0 # Total amount of focus.
var current_focus_recovery_rate = 1.5 # Percent of your Focus Pool that you regenerate each second while in combat.
var current_focus_cost_reduction = 10.0 # The focus costs of your actions are reduced by this percentage.
var current_reflect_chance = 10.0 # Your chance to return damage dealt to you back to the foe at a reduced value.
var current_reflect_damage = 10.0 # Percent of damage that is dealt to a foe by a Reflect.
var current_intensity = 10.0 # Your outgoing healing amount and focus costs are increased by this percent.

export var base_interrupt_armor = 1 # Your base interrupt armor
export var moment_of_opportunity_ratio = 1.25 # Percent damage when in MOO

export var experience_on_death = 1

var motion = Vector2()

var health = 5000.0 setget set_health
var shield = 100.0 setget set_shield
var focus = base_focus_pool
var dodge_pool = 20.0

var interrupt_armor = base_interrupt_armor
var is_interruptible = true
var is_interrupted = false
var is_interrupt_invulnerable = false

var is_in_combat = false

# Experience stuff
var current_experience = 0
var experience_to_level_up = round(pow(current_level,1.8) + current_level * 4.0)

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func compute_sprite_direction():
	if motion.x > 0:
		$ActorSprite.scale.x = 1
	elif motion.x < 0:
		$ActorSprite.scale.x = -1
	pass
	
func compute_animation():
	if abs(motion.x) < 0.05 and abs(motion.y) <0.05:
		$AnimationPlayer.play("Idle")
	else:
		$AnimationPlayer.play("Walk")
	if is_interrupted:
		# Stunned
		pass
	pass

func set_health(value):
	if value > health:
		# HEAL
		if value > current_max_health:
			value = current_max_health
			
		var total_heal = value - health
		
		health = health + total_heal
		
		var ftext = floating_damage.instance()
		ftext.value = -total_heal
		ftext.global_position = global_position
		$WorldPattern.add_child(ftext)
		
		if health > current_max_health:
			health = current_max_health
		
		pass
	else:
		# DAMAGE
		var current_health = health
		var current_shield = shield
		
		var total_damage = (health - value)
		if is_interrupted:
			total_damage = total_damage * moment_of_opportunity_ratio
		var health_damage = (100 - base_shield_mitigation) * total_damage / 100.0
		var shield_damage = (base_shield_mitigation) * total_damage / 100.0
		#print("" + str(total_damage) + " " + str(health_damage) + " " + str(shield_damage))
		
		if shield_damage > shield:
			# Not enouth shield
			health_damage = health_damage + shield_damage - shield
			shield = 0
			pass
		else:
			shield = shield - shield_damage
			pass
		
		health = health - health_damage
		$Camera2D.shake_amount = 0.1
		
		if value < health:
			$Tween.interpolate_property($ActorSprite, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.interpolate_property($ActorSprite, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
			$Tween.start()
		
		var ftext = floating_damage.instance()
		ftext.value = current_health - health
		ftext.global_position = global_position
		$WorldPattern.add_child(ftext)
		if (current_shield - shield) > 0:
			var ftext2 = floating_damage.instance()
			ftext2.value = current_shield - shield
			ftext2.color = Color(0,0,1)
			ftext2.global_position = global_position
			$WorldPattern.add_child(ftext2)
		
		$ShieldTimer.stop()
		$ShieldRestartTimer.stop()
		$ShieldRestartTimer.wait_time = base_reboot_time
		$ShieldRestartTimer.start()
		
		
		if health > current_max_health:
			health = current_max_health
		pass
	
	
	if health <= 0:
		#emit_signal('died')
		on_dead()
	pass

func set_health_with_crit(value, is_crit):
	if value > health:
		# HEAL
		if value > current_max_health:
			value = current_max_health
			
		var total_heal = value - health
		
		health = health + total_heal
		
		var ftext = floating_damage.instance()
		ftext.value = -total_heal
		ftext.global_position = global_position
		if is_crit:
			ftext.target_scale = Vector2(2,2)
		$WorldPattern.add_child(ftext)
		
		if health > current_max_health:
			health = current_max_health
		
		pass
	else:
		# DAMAGE
		var current_health = health
		var current_shield = shield
		
		var total_damage = (health - value)
		if is_interrupted:
			total_damage = total_damage * moment_of_opportunity_ratio
		var health_damage = (100 - base_shield_mitigation) * total_damage / 100.0
		var shield_damage = (base_shield_mitigation) * total_damage / 100.0
		#print("" + str(total_damage) + " " + str(health_damage) + " " + str(shield_damage))
		
		if shield_damage > shield:
			# Not enouth shield
			health_damage = health_damage + shield_damage - shield
			shield = 0
			pass
		else:
			shield = shield - shield_damage
			pass
		
		health = health - health_damage
		$Camera2D.shake_amount = 0.1
		
		if value < health:
			$Tween.interpolate_property($ActorSprite, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.interpolate_property($ActorSprite, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.05)
			$Tween.start()
		
		var ftext = floating_damage.instance()
		ftext.value = current_health - health
		ftext.global_position = global_position
		if is_crit:
			ftext.target_scale = Vector2(2,2)
		$WorldPattern.add_child(ftext)
		if (current_shield - shield) > 0:
			var ftext2 = floating_damage.instance()
			ftext2.value = current_shield - shield
			ftext2.color = Color(0,0,1)
			ftext2.global_position = global_position
			if is_crit:
				ftext2.target_scale = Vector2(2,2)
			$WorldPattern.add_child(ftext2)
		
		$ShieldTimer.stop()
		$ShieldRestartTimer.stop()
		$ShieldRestartTimer.wait_time = base_reboot_time
		$ShieldRestartTimer.start()
		
		
		if health > current_max_health:
			health = current_max_health
		pass
	
	
	if health <= 0:
		#emit_signal('died')
		on_dead()
	pass

func on_dead():
	EventBus.emit_signal("player_gain_xp", experience_on_death)
	queue_free()
	pass

func set_shield(value):
	if value > shield:
		# HEAL
		if value > current_max_shield:
			value = current_max_shield

		var total_shield = value - shield
		
		shield = shield + total_shield
			
		var ftext = floating_damage.instance()
		ftext.value = total_shield
		ftext.color = Color(0,0,0.7)
		ftext.global_position = global_position
		$WorldPattern.add_child(ftext)
		
		if shield > current_max_shield:
			shield = current_max_shield
		
		pass
	pass
	
func set_shield_with_crit(value, is_crit):
	if value > shield:
		# HEAL
		if value > current_max_shield:
			value = current_max_shield

		var total_shield = value - shield
		
		shield = shield + total_shield
			
		var ftext = floating_damage.instance()
		ftext.value = total_shield
		ftext.color = Color(0,0,0.7)
		ftext.global_position = global_position
		if is_crit:
			ftext.target_scale = Vector2(2,2)
		$WorldPattern.add_child(ftext)
		
		if shield > current_max_shield:
			shield = current_max_shield
		
		pass
	pass
	
func deflected():
	var ftext = floating_damage.instance()
	ftext.value = "Deflect"
	ftext.color = Color(0.7,0,0)
	ftext.global_position = global_position
	ftext.scale = Vector2(0.7,0.7)
	$WorldPattern.add_child(ftext)
	pass
	
func dodge(current_motion):
	if dodge_pool >= 10:
		current_motion = current_motion * 25
		dodge_pool = dodge_pool - 10
	return current_motion

func is_interruptible():
	if is_interruptible and not is_interrupt_invulnerable:
		return true
	else:
		return false
	pass
	
func stun():
	if is_interruptible():
		print(name + " stun " + str(interrupt_armor))
		if interrupt_armor == base_interrupt_armor:
			$InterruptArmorTimer.start()
			pass
		if interrupt_armor <= 0:
			is_interrupted = true
			is_interrupt_invulnerable = true
			$InterruptInvulnerabilityTimer.start()
			$MOOTimer.start()
			print(name + " interrupted")
		interrupt_armor = interrupt_armor - 1
	pass

func _on_DodgePoolTimer_timeout():
	if dodge_pool < 20.0:
		dodge_pool = dodge_pool + (3 / 10.0)
		
	if dodge_pool > 20.0:
		dodge_pool = 20.0
	pass # Replace with function body.


func _on_ShieldTimer_timeout():
	var not_combat_multiplier = 0
	if not is_in_combat:
		not_combat_multiplier = 1
		
	$ShieldTimer.wait_time = 1.0 / current_tick_time
	if shield < current_max_shield:
		shield = shield + (current_max_shield * current_regen_rate / 100.0)
		
	if shield > current_max_shield:
		shield = current_max_shield
		
	#if not in combat, restore health too

	if health < current_max_health:
		var health_amount = (current_max_health * current_focus_recovery_rate / 100.0) / 10.0
		health = health + (health_amount * not_combat_multiplier * 1.0)
		
	if health > current_max_health:
		health = current_max_health
		
	pass # Replace with function body.


func _on_FocusTimer_timeout():
	var not_combat_multiplier = 0
	if not is_in_combat:
		not_combat_multiplier = 1
	if focus < current_focus_pool:
		var focus_amount = (current_focus_pool * current_focus_recovery_rate / 100.0) / 10.0
		focus = focus + focus_amount + (focus_amount * not_combat_multiplier * 2.0)
		
	if focus > current_focus_pool:
		focus = current_focus_pool
	pass # Replace with function body.


func _on_ShieldRestartTimer_timeout():
	$ShieldTimer.start()
	pass # Replace with function body.


func _on_InterruptArmorTimer_timeout():
	interrupt_armor = base_interrupt_armor + global.player_number
	print(name + " interrupt armor restored")
	pass # Replace with function body.


func _on_MOOTimer_timeout():
	is_interrupted = false
	print(name + " interrupted stopped")
	pass # Replace with function body.

func get_MOO_time():
	return $MOOTimer.get_time_left()
	pass


func _on_InterruptInvulnerabilityTimer_timeout():
	print(name + " interrupted invulnerable stopped")
	is_interrupt_invulnerable = false
	pass # Replace with function body.


func _on_InCombatCheckTimer_timeout():
	if is_in_group("enemy"):
		is_in_combat = false
	else:
		var overlapping_areas = $InCombatArea.get_overlapping_bodies()
		#if overlapping_areas.size() > 0:
		is_in_combat = false
		for area in overlapping_areas:
			is_in_combat = true
		#else:
		#	print("NOT in combat")
		#	is_in_combat = false
	pass # Replace with function body.

