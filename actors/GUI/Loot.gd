extends Area2D

#var test = preload("res://level/GUI/LootObject.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasLayer/LootPanel.visible = false
	$CanvasLayer/LootPanel.rect_global_position = global.loot_panel_position
#	var test2 = test.instance()
#	$CanvasLayer/LootPanel/ScrollContainer/VBoxContainer.add_child(test2)
#	test2 = test.instance()
#	$CanvasLayer/LootPanel/ScrollContainer/VBoxContainer.add_child(test2)
#	test2 = test.instance()
#	$CanvasLayer/LootPanel/ScrollContainer/VBoxContainer.add_child(test2)
	$CanvasLayer/LootPanel/TopBar.change_draggable(true)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func add_item(item):
	$CanvasLayer/LootPanel/ScrollContainer/VBoxContainer.add_child(item)
	pass
	
func remove_item():
	pass


func _on_Loot_body_entered(body):
	$AnimationPlayer.play("opening")
	$CanvasLayer/LootPanel.visible = true
	pass # Replace with function body.


func _on_Loot_body_exited(body):
	$AnimationPlayer.play("closing")
	$CanvasLayer/LootPanel.visible = false
	pass # Replace with function body.


func _on_CleanTimer_timeout():
	var node_list = $CanvasLayer/LootPanel/ScrollContainer/VBoxContainer.get_children()
	
	for slot in node_list:
		var subnode_list = slot.get_node("LootImage").get_children()
		for subslot in subnode_list:
			if subslot.get_children().size() < 1 and (ItemManagement.holding_item == null):
				$CanvasLayer/LootPanel/ScrollContainer/VBoxContainer.remove_child(slot)
			pass
		pass
		
	if node_list.size() < 1:
		queue_free()
	pass # Replace with function body.
	
	
	
	
