extends NinePatchRect

const ItemClass = preload("res://items/item.gd");

onready var itemNameLabel = get_node("VBoxContainer/Item Name");
onready var itemValueLabel = get_node("VBoxContainer/Item Value");

func display(_item : ItemClass):
	visible = true;
	itemNameLabel.set_text(_item.itemName);
	itemValueLabel.text = ""
	print(_item.stats)
	for stat in _item.stats:
		print(stat)
		itemValueLabel.text = itemValueLabel.text + global.STATS_TO_TEXT[stat[0]] + ": " + str(stat[1]) + "\n"
	rect_size = Vector2(192, 128);
	rect_global_position = get_global_mouse_position() + Vector2(5, 5)#Vector2(mousePos.x + 5, mousePos.y + 5);
