extends Node2D

onready var _lifebar = $LifeBar

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	global_rotation = 0
	$LifeBar.value = get_parent().health
	$LifeBar.max_value = get_parent().current_max_health
	$ShieldBar.value = get_parent().shield
	$ShieldBar.max_value = get_parent().current_max_shield
	$FocusBar.value = get_parent().focus
	$FocusBar.max_value = get_parent().current_focus_pool
	
	# Dodge bar
	if get_parent().dodge_pool > 19.9:
		$DodgeBar.visible = false
	else:
		$DodgeBar.visible = true
	$DodgeBar.value = get_parent().dodge_pool
	$DodgeBar.max_value = 20.0
	
	# Moment of Opportunity
	if get_parent().get_MOO_time() < 0.1:
		$MOOBar.visible = false
		$LifeBar.modulate = Color("ffffff")
		#$LifeBar.get("custom_styles/fg").bg_color = Color("c70eef16")
		#$LifeBar.get("custom_styles/fg").bg_color = Color(0,0,0) #
	else:
		$MOOBar.visible = true
		$LifeBar.modulate = Color("d900ff") #
		#$LifeBar.get("custom_styles/fg").bg_color = Color(0,0,0)
		#var styleBox = $LifeBar.get("custom_styles/fg")
		#styleBox.bg_color = Color(1, 1, 1)
	$MOOBar.value = get_parent().get_MOO_time()
	$MOOBar.max_value = get_parent().get_node("MOOTimer").wait_time
	
	# Interrupt armor counter
	if get_parent().interrupt_armor < 1 or not get_parent().is_interruptible():
		$InterruptArmor.visible = false
	else:
		$InterruptArmor.visible = true
	$InterruptArmor.text = str(get_parent().interrupt_armor)
	
	pass
