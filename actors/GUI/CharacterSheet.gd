extends Panel

# Called when the node enters the scene tree for the first time.
func _ready():
	var children = $HBoxContainer/Panel/LeftSlots.get_children()
	for slot in children:
		slot.connect("mouse_entered", ItemManagement, "mouse_enter_slot", [slot]);
		slot.connect("mouse_exited", ItemManagement, "mouse_exit_slot", [slot]);
		slot.connect("gui_input", ItemManagement, "slot_gui_input", [slot]);
		
	children = $HBoxContainer/Panel/RightSlots.get_children()
	for slot in children:
		slot.connect("mouse_entered", ItemManagement, "mouse_enter_slot", [slot]);
		slot.connect("mouse_exited", ItemManagement, "mouse_exit_slot", [slot]);
		slot.connect("gui_input", ItemManagement, "slot_gui_input", [slot]);
	pass # Replace with function body.

func _process(delta):
	$HBoxContainer/Panel/AssaultLabel/AssaultValue.text = str(get_parent().get_parent().current_assault_power)
	$HBoxContainer/Panel/SupportLabel/SupportValue.text = str(get_parent().get_parent().current_support_power)
	$HBoxContainer/Panel/HealthLabel/HealthValue.text = str(get_parent().get_parent().current_max_health)
	$HBoxContainer/Panel/ShieldLabel/ShieldValue.text = str(get_parent().get_parent().current_max_shield)
	pass

func set_visible_or_not():
	if visible:
		visible = false
	else:
		visible = true
	$TopBar.change_draggable(visible)
