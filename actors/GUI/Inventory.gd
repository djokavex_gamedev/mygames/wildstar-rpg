extends Panel

const ItemSlotClass = preload("res://items/ItemSlot.gd");

const MAX_SLOTS = 45;

var slotList = Array();

var holdingItem = null;
var itemOffset = Vector2(0, 0);

# Called when the node enters the scene tree for the first time.
func _ready():
	var slots = get_node("SlotsContainer/Slots");
	
	var icon = load("res://assets/img/equipment/Sword.png")
	
	for _i in range(MAX_SLOTS):
		var slot = ItemSlotClass.new()
		slot.connect("mouse_entered", ItemManagement, "mouse_enter_slot", [slot]);
		slot.connect("mouse_exited", ItemManagement, "mouse_exit_slot", [slot]);
		slot.connect("gui_input", ItemManagement, "slot_gui_input", [slot]);
		
#		var test5 = preload("res://items/item.gd").new("Sword", icon, null, 12, 12)
#		slot.setItem(test5)
		
		slotList.append(slot);
		slots.add_child(slot);
		
		#var test5 = preload("res://items/item.gd").new("Sword", icon, null, 12, 12)
#		test5.set_name("LootObject")
#		test4.get_node("LootImage").add_child(test5)
#		test4.get_node("LootName").text = test4.get_node("LootImage/LootObject").itemName + " " + global.STATS_TO_TEXT[test4.get_node("LootImage/LootObject").stats[0][0]] + " " + str(test4.get_node("LootImage/LootObject").stats[0][1])
		
		#slots.add_child(test5);
	pass # Replace with function body.

func set_visible_or_not():
	if visible:
		visible = false
		var player_tooltip = get_node("/root/TestLevel/MyPlayer/PlayerMenuPanels/Tooltip");
		player_tooltip.visible = false
	else:
		visible = true
	$TopBar.change_draggable(visible)

func getFreeSlot():
	for slot in slotList:
		if !slot.item:
			return slot;
