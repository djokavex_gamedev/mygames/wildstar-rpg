extends "res://actors/Enemies/Enemy.gd"

var bullet = preload("res://spells/Enemies/BulletGravity/BulletGravity.tscn")
var zone_on_player = preload("res://spells/Enemies/ZoneOnPlayer/ZoneOnPlayer.tscn")
var cross_zone = preload("res://spells/Enemies/CrossZone/CrossZone.tscn")

var number_of_phase = 0

func specific_ready():
	$ActorGui/FocusBar.visible = false
	is_interruptible = false
	$CollisionShape2D/AnimationPlayer.play("Turn")
	pass
	
func _process(delta):
	if not aggroed:
		$TimerSwitchAttack.stop()
		var overlapping_areas = $AggroArea.get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				aggroed = true
				
	else:
		if $TimerSwitchAttack.is_stopped():
			$TimerSwitchAttack.start()
			_on_TimerSwitchAttack_timeout()
		global_rotation = 0
		if $WorldPattern.get_node_or_null("CrossZone") != null:
			motion = Vector2(0,0)
		else:
			if get_parent().get_node_or_null("MyPlayer") != null:
				look_at(get_parent().get_node("MyPlayer").position)
				motion = Vector2(1.0,0.0).normalized() * 500
				motion = motion.rotated(rotation)
				compute_sprite_direction()
			global_rotation = 0
			
		move_and_slide(motion*delta)
	
	
	compute_animation()
	pass

func _on_Attack_timeout():
	var speed = 160
	if get_parent().get_node_or_null("MyPlayer") != null:
		look_at(get_parent().get_node("MyPlayer").position)
	var bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(1.0,0.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(-1.0,0.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(0.0,1.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(0.0,-1.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	
	bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(1.0,1.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(-1.0,-1.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(-1.0,1.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	bull = bullet.instance()
	bull.global_position = self.global_position
	bull.motion = Vector2(1.0,-1.0).normalized() * speed
	bull.motion = bull.motion.rotated(rotation)
	bull.linear_velocity = bull.motion
	$WorldPattern.add_child(bull)
	
	pass # Replace with function body.


func _on_Attack2_timeout():
	var save_nodes = get_tree().get_nodes_in_group("player")
	if save_nodes.size() > 0:
		randomize()
		var ii = randi() % save_nodes.size()
		#print(str(ii)+" "+str(save_nodes.size()))
		
		var zone = zone_on_player.instance()
		zone.global_position = save_nodes[ii].global_position - self.global_position
		zone.target_player = save_nodes[ii]
		$WorldPattern.add_child(zone)
	pass # Replace with function body.


func _on_TimerSwitchAttack_timeout():
	var crosszone = cross_zone.instance()
	crosszone.global_position = self.global_position
	crosszone.enable_rotation = true
	crosszone.rotation_speed = crosszone.rotation_speed + number_of_phase * 0.1
	
	#zone.target_player = save_nodes[ii]
	$WorldPattern.add_child(crosszone)
	
	if $Attack.is_stopped():
		$Attack.start()
		$Attack2.stop()
	else:
		$Attack2.start()
		$Attack.stop()
		
	number_of_phase = number_of_phase + 1 
	pass # Replace with function body.
