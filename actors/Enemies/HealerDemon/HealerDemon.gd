extends "res://actors/Enemies/Enemy.gd"

var bullet = preload("res://spells/Enemies/BulletGravity/BulletGravity.tscn")
var zone_on_player = preload("res://spells/Enemies/ZoneOnPlayer/ZoneOnPlayer.tscn")
var front_swing = preload("res://spells/Enemies/FrontSwing/FrontSwing.tscn")
var big_heal = preload("res://spells/Enemies/BigAOEHeal/BigAOEHeal.tscn")

func specific_ready():
	$ActorGui/FocusBar.visible = false
	$CollisionShape2D/AnimationPlayer.play("Turn")
	$HealTimer.stop()
	$SmallShotTimer.stop()
	pass
	
func _process(delta):
	if not aggroed:
		var overlapping_areas = $AggroArea.get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				aggroed = true
	
	else:
		if $HealTimer.is_stopped():
			$HealTimer.start()
		if $SmallShotTimer.is_stopped():
			$SmallShotTimer.start()
		global_rotation = 0
		if $WorldPattern.get_node_or_null("BigAOEHeal") != null:
			motion = Vector2(0.0,0.0)
		else:
			if get_parent().get_node_or_null("MyPlayer") != null:
				look_at(get_parent().get_node("MyPlayer").position)
				var speed = min(position.distance_to(get_parent().get_node("MyPlayer").position)*15, 2000)
				if position.distance_to(get_parent().get_node("MyPlayer").position) < 30:
					speed = 0
				motion = Vector2(1.0,0.0).normalized() * speed
				motion = motion.rotated(rotation)
				compute_sprite_direction()
			else:
				motion = Vector2(0,0)
			global_rotation = 0
			
		if is_interrupted:
			if $WorldPattern.get_node_or_null("BigAOEHeal") != null:
				$WorldPattern.get_node("BigAOEHeal").queue_free()
			motion = Vector2(0,0)
			#$SmallShotTimer.stop()
		else:
			pass
				
		move_and_slide(motion*delta)
		
		
	compute_animation()
	pass


func _on_HealTimer_timeout():
	if is_interrupted:
		$HealTimer.wait_time = 1
		$HealTimer.start()
	else:
		$HealTimer.wait_time = 12
		$HealTimer.start()
		interrupt_armor = base_interrupt_armor
		var bigheal = big_heal.instance()
		bigheal.global_position = self.global_position
		$WorldPattern.add_child(bigheal)
	
	pass # Replace with function body.


func _on_SmallShotTimer_timeout():
	var speed = 200
	if get_parent().get_node_or_null("MyPlayer") != null:
		look_at(get_parent().get_node("MyPlayer").position)
		var bull = bullet.instance()
		bull.global_position = self.global_position
		bull.motion = Vector2(1.0,0.0).normalized() * speed
		bull.motion = bull.motion.rotated(rotation)
		bull.linear_velocity = bull.motion
		$WorldPattern.add_child(bull)
	pass # Replace with function body.
