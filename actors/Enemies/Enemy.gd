extends "res://actors/Actor.gd"

const ItemSlotClass = preload("res://items/ItemSlot.gd");

var loot_table = load("res://items/Loot/LootTable.gd")
var table = loot_table.new()

var aggroed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	add_group()
	enemy_ready()
	specific_ready()
	pass # Replace with function body.

func add_group():
	add_to_group("enemy")

func enemy_ready():
	compute_stats()
	table.add_item("Sword",25)
	table.add_item("HeavyArmor",25)
	table.add_item("HeavyBoots",25)
	table.add_item("HeavyGloves",25)
	table.add_item("Shield",25)
	table.add_item("Necklace",25)
	table.add_item("Ring",25)
	table.roll(3)
	pass
	
func specific_ready():
	pass

func compute_stats():
	health = (base_max_health + round(pow(current_level, 2.0)+current_level*20)) * global.player_number
	shield = (base_max_shield + round(pow(current_level, 1.7)+current_level*10)) * global.player_number
	interrupt_armor = base_interrupt_armor + global.player_number
	

	current_assault_power = base_assault_power + round(pow(current_level, 1.5)+current_level*1.5)
	current_assault_power = current_assault_power + current_assault_power * (global.player_number - 1) * 0.1
	current_support_power = base_support_power + round(pow(current_level, 1.5)+current_level*1.5)
	current_support_power = current_support_power + current_support_power * (global.player_number - 1) * 0.1
	current_max_health = (base_max_health + round(pow(current_level, 2.0)+current_level*20)) * global.player_number
	current_max_shield = (base_max_shield + round(pow(current_level, 1.7)+current_level*10)) * global.player_number
	# Offensive stats
	current_strikethrough = base_strikethrough
	current_crit_hit_chance = base_crit_hit_chance
	current_crit_hit_severity = base_crit_hit_severity
	current_multi_hit_chance = base_multi_hit_chance
	current_multi_hit_severity = base_multi_hit_severity
	current_vigor = base_vigor
	current_armor_pierce = base_armor_pierce
	# Defensive stats
	current_physical_mitigation = base_physical_mitigation
	current_technology_mitigation = base_technology_mitigation
	current_magic_mitigation = base_magic_mitigation
	current_glance_mitigation = base_glance_mitigation
	current_glance_chance = base_glance_chance
	current_critical_mitigation = base_critical_mitigation
	current_deflect_chance = base_deflect_chance
	current_deflect_crit_hit_chance = base_deflect_crit_hit_chance
	# Shield stats
	current_shield_mitigation = base_shield_mitigation
	current_regen_rate = base_regen_rate
	current_reboot_time = base_reboot_time
	current_tick_time = base_tick_time
	# Utility stats
	current_cooldown_reduction = base_cooldown_reduction
	current_cc_duration = base_cc_duration
	current_lifesteal = base_lifesteal
	current_focus_pool = base_focus_pool + round(pow(current_level, 1.7)+current_level*10)
	current_focus_recovery_rate = base_focus_recovery_rate
	current_focus_cost_reduction = base_focus_cost_reduction
	current_reflect_chance = base_reflect_chance
	current_reflect_damage = base_reflect_damage
	current_intensity = base_intensity
	
	pass

func create_loot():
	# Loot
	var item_in_loot = false
	var test = load("res://actors/GUI/Loot.tscn")
	var test2 = test.instance()
	test2.global_position = global_position
	
	# Item in loot
#	if 50 > (randi() % 100):
#		item_in_loot = true
#		var test3 = load("res://level/GUI/LootObject.tscn")
#		var test4 = test3.instance()
#		test2.add_item(test4)
	
	for i in table.output_data.size():
		print(table.output_data[i])
		item_in_loot = true
		var test3 = load("res://level/GUI/LootObject.tscn")
		var test4 = test3.instance()
		
		var icon = load("res://assets/img/equipment/"+table.output_data[i]+".png")
		
		var slot = ItemSlotClass.new()
		slot.connect("mouse_entered", ItemManagement, "mouse_enter_slot", [slot]);
		slot.connect("mouse_exited", ItemManagement, "mouse_exit_slot", [slot]);
		slot.connect("gui_input", ItemManagement, "slot_gui_input", [slot]);
		
		#var test5 = preload("res://items/item.gd").new("Sword", icon, null, 12, 12)
		var test5 = preload("res://items/item.gd").new(table.output_data[i], icon, null, 12, ItemManagement.ItemNameToSlot[table.output_data[i]])
		test5.set_name("LootObject")
		slot.setItem(test5)
		
		#slotList.append(slot);
		#slots.add_child(slot);
		
		
		test4.get_node("LootImage").add_child(slot)
		test4.get_node("LootName").text = test5.itemName + " " + global.STATS_TO_TEXT[test5.stats[0][0]] + " " + str(test5.stats[0][1])
		
		test2.add_item(test4)
		
	if item_in_loot:
		get_tree().get_root().get_node("TestLevel").add_child(test2)
	pass
