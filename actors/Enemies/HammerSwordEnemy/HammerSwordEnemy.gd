extends "res://actors/Enemies/Enemy.gd"

var bullet = preload("res://spells/Enemies/BulletGravity/BulletGravity.tscn")
var zone_on_player = preload("res://spells/Enemies/ZoneOnPlayer/ZoneOnPlayer.tscn")
var front_swing = preload("res://spells/Enemies/FrontSwing/FrontSwing.tscn")
var jump_hammer_sword = preload("res://spells/Enemies/JumpHammerSword/JumpHammerSword.tscn")
var tourbilol = preload("res://spells/Enemies/Tourbilol/Tourbilol.tscn")

var attack_sequence = 0
export var attack_move = false
var attack_position = Vector2(0, 0)

func specific_ready():
	$ActorGui/FocusBar.visible = false
	$CollisionShape2D/AnimationPlayer.play("Turn")
	
	print(str(health) + "/" + str(current_max_health))
	pass
	
func _process(delta):
	#print(str(health) + "/" + str(current_max_health))
	if not aggroed:
		#$SmallShotTimer.stop()
		var overlapping_areas = $AggroArea.get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				aggroed = true
	
	else:
		global_rotation = 0
		if $WorldPattern.get_node_or_null("JumpHammerSword") != null or get_node_or_null("FrontSwing") != null or get_node_or_null("Tourbilol") != null:
			motion = Vector2(0.0,0.0)
			if get_parent().get_node_or_null("MyPlayer") != null:
				if get_parent().get_node("MyPlayer").position.x > position.x: 
					$ActorSprite.scale.x = 1
				else:
					$ActorSprite.scale.x = -1
			if attack_move:
				global_position = lerp(global_position, attack_position, 0.2)
			if $WorldPattern/WeaponSprite.visible == true:
				$WorldPattern/WeaponSprite.position = lerp($WorldPattern/WeaponSprite.position, attack_position, 0.3)
		else:
			if get_parent().get_node_or_null("MyPlayer") != null:
				look_at(get_parent().get_node("MyPlayer").position)
				var speed = min(position.distance_to(get_parent().get_node("MyPlayer").position)*30, 5000)
				if position.distance_to(get_parent().get_node("MyPlayer").position) < 30:
					speed = 0
				motion = Vector2(1.0,0.0).normalized() * speed
				motion = motion.rotated(rotation)
			else:
				motion = Vector2(0,0)
			compute_sprite_direction()
			global_rotation = 0
		
		if is_interrupted:
			motion = Vector2(0,0)
			#$SmallShotTimer.stop()
		else:
			#if $SmallShotTimer.is_stopped():
			#	$SmallShotTimer.start()
			pass
		move_and_slide(motion*delta)
	
	if $AnimationPlayer.current_animation != "Walk" and $AnimationPlayer.current_animation != "Idle" and $AnimationPlayer.is_playing():
		pass
	else:
		compute_animation()
	pass


func _on_TimerAttack_timeout():
	attack_move = false
	if attack_sequence == 10:
		attack_sequence = 0
		
	match attack_sequence:
		0:
			normal_attack()
		1:
			tourbilol_attack()
		2:
			jump_attack()
		3:
			tourbilol_attack()
		4:
			normal_attack()
		5:
			jump_attack()
		6:
			normal_attack()
		7:
			tourbilol_attack()
		8:
			jump_attack()
		9:
			tourbilol_attack()
		_:
			normal_attack()
	
	attack_sequence = attack_sequence + 1
	pass # Replace with function body.

func normal_attack():
	print("attack")
	if get_parent().get_node_or_null("MyPlayer") != null:
		if position.distance_to(get_parent().get_node("MyPlayer").position) < 120:
			look_at(get_parent().get_node("MyPlayer").position)
			if position.distance_to(get_parent().get_node("MyPlayer").position) > 30:
				var speed = min(position.distance_to(get_parent().get_node("MyPlayer").position)*50, 10000)
				attack_position = (global_position + 2.0*get_parent().get_node("MyPlayer").position) / 3.0
			else:
				attack_position = global_position
			var bull = front_swing.instance()
			bull.rotation = rotation
			add_child(bull)
			$AnimationPlayer.play("attack")
	pass
	
func jump_attack():
	print("attack jump")
	if get_parent().get_node_or_null("MyPlayer") != null:
		look_at(get_parent().get_node("MyPlayer").position)
		attack_position = get_parent().get_node("MyPlayer").position
		var bull = jump_hammer_sword.instance()
		bull.rotation = rotation
		bull.player = get_parent().get_node("MyPlayer")
		if $ActorSprite.scale.x == 1:
			$WorldPattern/WeaponSprite.position = global_position + Vector2(32,-48)
		else:
			$WorldPattern/WeaponSprite.position = global_position + Vector2(-32,-48)
		$WorldPattern.add_child(bull)
		$AnimationPlayer.play("attack_jump")
	pass

func tourbilol_attack():
	print("attack tourbilol")
	if get_parent().get_node_or_null("MyPlayer") != null:
		if position.distance_to(get_parent().get_node("MyPlayer").position) < 120:
			look_at(get_parent().get_node("MyPlayer").position)
			var bull = tourbilol.instance()
			bull.rotation = rotation
			add_child(bull)
			$AnimationPlayer.play("tourbilol")
	pass
	
func play_ground_arrival():
	audio_manager.play_sfx(load("res://assets/audio/sfx/explosionCrunch_000.ogg"), 1, 1)
	pass
func play_ground_arrival2():
	audio_manager.play_sfx(load("res://assets/audio/sfx/lowFrequency_explosion_001.ogg"), 1, 1)
	pass
