extends "res://actors/Actor.gd"

export(String, FILE, "*.tscn") var action1
export(String, FILE, "*.tscn") var action2
export(String, FILE, "*.tscn") var action3
export(String, FILE, "*.tscn") var action4

var action1_spell = null
var action2_spell = null
var action3_spell = null
var action4_spell = null

var stun_preload = preload("res://spells/Players/Medic/Stun/Stun.tscn")
var stun_ability = null

# Called when the node enters the scene tree for the first time.
func _ready():
	add_group()
	EventBus.connect("player_gain_xp", self, "on_player_gain_xp")
	
	player_ready()
	specific_ready()
	pass # Replace with function body.

func add_group():
	add_to_group("player")

func player_ready():
	compute_stats()
	current_experience = 0
	experience_to_level_up = round(pow(current_level,1.8) + current_level * 4.0)
	
	health = base_max_health + round(pow(current_level, 2.0)+current_level*20)
	shield = base_max_shield + round(pow(current_level, 1.7)+current_level*10)
	interrupt_armor = base_interrupt_armor
	
	EventBus.connect("equipment_changed", self, "on_equipment_change")

	if action1 != null and action1 != "":
		action1_spell = load(action1).instance()
		if action1_spell.static_spell == true:
			$WorldPattern.add_child(action1_spell)
		else:
			add_child(action1_spell)
	if action2 != null and action2 != "":
		action2_spell = load(action2).instance()
		if action2_spell.static_spell == true:
			$WorldPattern.add_child(action2_spell)
		else:
			add_child(action2_spell)
	if action3 != null and action3 != "":
		action3_spell = load(action3).instance()
		if action3_spell.static_spell == true:
			$WorldPattern.add_child(action3_spell)
		else:
			add_child(action3_spell)
	if action4 != null and action4 != "":
		action4_spell = load(action4).instance()
		if action4_spell.static_spell == true:
			$WorldPattern.add_child(action4_spell)
		else:
			add_child(action4_spell)
	pass
	
func specific_ready():
	pass

func on_equipment_change():
	print("recompute stats")
	compute_stats()
	pass
	
func _unhandled_input(event):
	if event is InputEventKey or event is InputEventMouseButton:
		#print(event)
		if action1_spell != null and $GCD.is_stopped() and action1_spell.get_node("CD").is_stopped():
			if Input.is_action_pressed("p1_action1"):
				if action1_spell.static_spell == true:
					action1_spell.global_position = global_position
					action1_spell.global_rotation = global_rotation
				action1_spell.show_telegraph(true)
			else:
				action1_spell.show_telegraph(false)
			if Input.is_action_just_released("p1_action1"):
				if $GCD.is_stopped():
					if action1_spell.use_art():
						if action1_spell.static_spell == true:
							action1_spell.global_position = global_position
							action1_spell.global_rotation = global_rotation
						$GCD.wait_time = action1_spell.spell_gcd
						$GCD.start()
				pass
		
		if action2_spell != null and $GCD.is_stopped() and action2_spell.get_node("CD").is_stopped():
			if Input.is_action_pressed("p1_action2"):
				if action2_spell.static_spell == true:
					action2_spell.global_position = global_position
					action2_spell.global_rotation = global_rotation
				action2_spell.show_telegraph(true)
			else:
				action2_spell.show_telegraph(false)
			if Input.is_action_just_released("p1_action2"):
				if $GCD.is_stopped():
					if action2_spell.use_art():
						if action2_spell.static_spell == true:
							action2_spell.global_position = global_position
							action2_spell.global_rotation = global_rotation
						$GCD.wait_time = action2_spell.spell_gcd
						$GCD.start()
				pass
		
		if action3_spell != null and $GCD.is_stopped() and action3_spell.get_node("CD").is_stopped():
			if Input.is_action_pressed("p1_action3"):
				if action3_spell.static_spell == true:
					action3_spell.global_position = global_position
					action3_spell.global_rotation = global_rotation
				action3_spell.show_telegraph(true)
			else:
				action3_spell.show_telegraph(false)
			if Input.is_action_just_released("p1_action3"):
				if $GCD.is_stopped():
					if action3_spell.use_art():
						if action3_spell.static_spell == true:
							action3_spell.global_position = global_position
							action3_spell.global_rotation = global_rotation
						$GCD.wait_time = action3_spell.spell_gcd
						$GCD.start()
				pass
		
		if action4_spell != null and $GCD.is_stopped() and action4_spell.get_node("CD").is_stopped():
			if Input.is_action_pressed("p1_action4"):
				if action4_spell.static_spell == true:
					action4_spell.global_position = global_position
					action4_spell.global_rotation = global_rotation
				action4_spell.show_telegraph(true)
			else:
				action4_spell.show_telegraph(false)
			if Input.is_action_just_released("p1_action4"):
				if $GCD.is_stopped():
					if action4_spell.use_art():
						if action4_spell.static_spell == true:
							action4_spell.global_position = global_position
							action4_spell.global_rotation = global_rotation
						$GCD.wait_time = action4_spell.spell_gcd
						$GCD.start()
				pass
		
		if stun_ability != null and stun_ability.get_node("CD").is_stopped():
			if Input.is_action_pressed("p1_counter"):
				if stun_ability.static_spell == true:
					stun_ability.global_position = global_position
					stun_ability.global_rotation = global_rotation
				stun_ability.show_telegraph(true)
			else:
				stun_ability.show_telegraph(false)
			if Input.is_action_just_released("p1_counter"):
				#if $GCD.is_stopped():
				if stun_ability.use_art():
					if stun_ability.static_spell == true:
						stun_ability.global_position = global_position
						stun_ability.global_rotation = global_rotation
					$GCD.wait_time = stun_ability.spell_gcd
					$GCD.start()
				pass
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_home"):
		get_parent().get_node("InGameOptionMenu/ColorRect").visible = true
		pass
	if Input.is_action_just_pressed("p1_inventory"):
		$PlayerMenuPanels/Inventory.set_visible_or_not()
		pass
	if Input.is_action_just_pressed("p1_inventory"):
		$PlayerMenuPanels/CharacterSheet.set_visible_or_not()
		pass
	
	look_at(get_global_mouse_position())
	$ActorSprite.global_rotation = 0
	$CollisionShape2D.rotation = 0
	$CollisionShape2D.global_rotation = 0
	$CollisionShape2D.position = Vector2(0,5).rotated(-rotation)
	
	var vel = Vector2(0,0)
	vel.x = int(Input.is_action_pressed("p1_right")) - int(Input.is_action_pressed("p1_left"))
	vel.y = int(Input.is_action_pressed("p1_down")) - int(Input.is_action_pressed("p1_up"))
		
	var current_motion = vel.normalized() * 120
	if Input.is_action_just_pressed("p1_dodge"):
		current_motion = dodge(current_motion)
		#$DodgeTrail.emitting = true
	#$DodgeTrail.rotation = 0
	#$WorldPattern/DodgeTrail.global_position = global_position
		
	#$AnimationPlayer.play("Idle")
	compute_sprite_direction()
	
	motion = lerp(motion, current_motion, 0.2)
	move_and_slide(motion)
	compute_animation()
	
#	if action1_spell != null and $GCD.is_stopped() and action1_spell.get_node("CD").is_stopped():
#		if Input.is_action_pressed("p1_action1"):
#			if action1_spell.static_spell == true:
#				action1_spell.global_position = global_position
#				action1_spell.global_rotation = global_rotation
#			action1_spell.show_telegraph(true)
#		else:
#			action1_spell.show_telegraph(false)
#		if Input.is_action_just_released("p1_action1"):
#			if $GCD.is_stopped():
#				if action1_spell.use_art():
#					if action1_spell.static_spell == true:
#						action1_spell.global_position = global_position
#						action1_spell.global_rotation = global_rotation
#					$GCD.wait_time = action1_spell.spell_gcd
#					$GCD.start()
#			pass
#
#	if action2_spell != null and $GCD.is_stopped() and action2_spell.get_node("CD").is_stopped():
#		if Input.is_action_pressed("p1_action2"):
#			if action2_spell.static_spell == true:
#				action2_spell.global_position = global_position
#				action2_spell.global_rotation = global_rotation
#			action2_spell.show_telegraph(true)
#		else:
#			action2_spell.show_telegraph(false)
#		if Input.is_action_just_released("p1_action2"):
#			if $GCD.is_stopped():
#				if action2_spell.use_art():
#					if action2_spell.static_spell == true:
#						action2_spell.global_position = global_position
#						action2_spell.global_rotation = global_rotation
#					$GCD.wait_time = action2_spell.spell_gcd
#					$GCD.start()
#			pass
#
#	if action3_spell != null and $GCD.is_stopped() and action3_spell.get_node("CD").is_stopped():
#		if Input.is_action_pressed("p1_action3"):
#			if action3_spell.static_spell == true:
#				action3_spell.global_position = global_position
#				action3_spell.global_rotation = global_rotation
#			action3_spell.show_telegraph(true)
#		else:
#			action3_spell.show_telegraph(false)
#		if Input.is_action_just_released("p1_action3"):
#			if $GCD.is_stopped():
#				if action3_spell.use_art():
#					if action3_spell.static_spell == true:
#						action3_spell.global_position = global_position
#						action3_spell.global_rotation = global_rotation
#					$GCD.wait_time = action3_spell.spell_gcd
#					$GCD.start()
#			pass
#
#	if action4_spell != null and $GCD.is_stopped() and action4_spell.get_node("CD").is_stopped():
#		if Input.is_action_pressed("p1_action4"):
#			if action4_spell.static_spell == true:
#				action4_spell.global_position = global_position
#				action4_spell.global_rotation = global_rotation
#			action4_spell.show_telegraph(true)
#		else:
#			action4_spell.show_telegraph(false)
#		if Input.is_action_just_released("p1_action4"):
#			if $GCD.is_stopped():
#				if action4_spell.use_art():
#					if action4_spell.static_spell == true:
#						action4_spell.global_position = global_position
#						action4_spell.global_rotation = global_rotation
#					$GCD.wait_time = action4_spell.spell_gcd
#					$GCD.start()
#			pass
#
#	if stun_ability != null and stun_ability.get_node("CD").is_stopped():
#		if Input.is_action_pressed("p1_counter"):
#			if stun_ability.static_spell == true:
#				stun_ability.global_position = global_position
#				stun_ability.global_rotation = global_rotation
#			stun_ability.show_telegraph(true)
#		else:
#			stun_ability.show_telegraph(false)
#		if Input.is_action_just_released("p1_counter"):
#			#if $GCD.is_stopped():
#			if stun_ability.use_art():
#				if stun_ability.static_spell == true:
#					stun_ability.global_position = global_position
#					stun_ability.global_rotation = global_rotation
#				$GCD.wait_time = stun_ability.spell_gcd
#				$GCD.start()
#			pass
	
	specific_process(delta)
	
	pass
	
func specific_process(delta):
	pass
	
func compute_stats():
	current_assault_power = base_assault_power + round(pow(current_level, 1.5)+current_level*1.5)
	current_support_power = base_support_power + round(pow(current_level, 1.5)+current_level*1.5)
	current_max_health = base_max_health + round(pow(current_level, 2.0)+current_level*20)
	current_max_shield = base_max_shield + round(pow(current_level, 1.7)+current_level*10)
	# Offensive stats
	current_strikethrough = base_strikethrough
	current_crit_hit_chance = base_crit_hit_chance
	current_crit_hit_severity = base_crit_hit_severity
	current_multi_hit_chance = base_multi_hit_chance
	current_multi_hit_severity = base_multi_hit_severity
	current_vigor = base_vigor
	current_armor_pierce = base_armor_pierce
	# Defensive stats
	current_physical_mitigation = base_physical_mitigation
	current_technology_mitigation = base_technology_mitigation
	current_magic_mitigation = base_magic_mitigation
	current_glance_mitigation = base_glance_mitigation
	current_glance_chance = base_glance_chance
	current_critical_mitigation = base_critical_mitigation
	current_deflect_chance = base_deflect_chance
	current_deflect_crit_hit_chance = base_deflect_crit_hit_chance
	# Shield stats
	current_shield_mitigation = base_shield_mitigation
	current_regen_rate = base_regen_rate
	current_reboot_time = base_reboot_time
	current_tick_time = base_tick_time
	# Utility stats
	current_cooldown_reduction = base_cooldown_reduction
	current_cc_duration = base_cc_duration
	current_lifesteal = base_lifesteal
	current_focus_pool = base_focus_pool + round(pow(current_level, 1.7)+current_level*10)
	current_focus_recovery_rate = base_focus_recovery_rate
	current_focus_cost_reduction = base_focus_cost_reduction
	current_reflect_chance = base_reflect_chance
	current_reflect_damage = base_reflect_damage
	current_intensity = base_intensity
	
	add_stats_from_equipment()
	pass

func add_stats_from_equipment():
	var slots = $PlayerMenuPanels/CharacterSheet/HBoxContainer/Panel/LeftSlots.get_children()
	slots += $PlayerMenuPanels/CharacterSheet/HBoxContainer/Panel/RightSlots.get_children()
	for slot in slots:
		if slot.item == null:
			print("No stuff on slot " + str(slot.slotType))
			pass
		else:
			print("Stuff on slot " + str(slot.slotType))
			for stat in slot.item.stats:
				match stat[0]:
					global.ASSAULT_POWER:
						current_assault_power = current_assault_power + stat[1]
						pass
					global.SUPPORT_POWER:
						current_support_power = current_support_power + stat[1]
						pass
					global.MAX_HEALTH:
						current_max_health = current_max_health + stat[1]
						pass
					global.MAX_SHIELD:
						current_max_shield = current_max_shield + stat[1]
						pass
					global.STRIKETHROUGH:
						current_strikethrough = current_strikethrough + stat[1]
						pass
					global.CRIT_CHANCE:
						current_crit_hit_chance = current_crit_hit_chance + stat[1]
						pass
					global.CRIT_SEVERITY:
						current_crit_hit_severity = current_crit_hit_severity + stat[1]
						pass
					global.MULTI_CHANCE:
						pass
					global.MULTI_SEVERITY:
						pass
					global.VIGOR:
						pass
					global.ARMOR_PIERCE:
						pass
					global.PHYSICAL_MITIGATION:
						pass
					global.TECHNOLOGY_MITIGATION:
						pass
					global.MAGIC_MITIGATION:
						pass
					global.GLANCE_MITIGATION:
						pass
					global.GLANCE_CHANCE:
						pass
					global.CRITICAL_MITIGATION:
						pass
					global.DEFLECT_CHANCE:
						pass
					global.DEFLECT_CRIT_CHANCE:
						pass
					global.SHIELD_MITIGATION:
						pass
					global.SHIELD_RATE:
						pass
					global.SHIELD_REBOOT_TIME:
						pass
					global.TICK_TIME:
						pass
					global.CD_REDUCTION:
						pass
					global.LIFESTEAL:
						pass
					global.FOCUS_POOL:
						pass
					global.FOCUS_RECOV_RATE:
						pass
					global.FOCUS_COST_REDUCTION:
						pass
					global.REFLECT_CHANCE:
						pass
					global.REFLECT_DAMAGE:
						pass
					global.INTENSITY:
						pass
				pass
			pass
	pass
	
func get_GCD_value():
	return $GCD.get_time_left()
	pass

func on_player_gain_xp(experience):
	print("Current XP: " + str(current_experience) + "/" + str(experience_to_level_up))
	print("XP gained: "+str(experience))
	current_experience = current_experience + experience
	if current_experience > experience_to_level_up:
		# Level UP
		current_level = current_level + 1
		experience_to_level_up = experience_to_level_up + round(pow(current_level,1.8) + current_level * 4.0)
		compute_stats()
		EventBus.emit_signal("player_level_up")
	pass

func on_dead():
	print("Looser")
	queue_free()
	pass

