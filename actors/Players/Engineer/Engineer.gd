extends "res://actors/Players/Player.gd"

#var stun_preload = preload("res://spells/Players/Medic/Stun/Stun.tscn")
#var stun_ability = null

# Called when the node enters the scene tree for the first time.
func specific_ready():
	$ActorGui/FocusBar.visible = true
	stun_ability = stun_preload.instance()
	add_child(stun_ability)
	$CollisionShape2D/AnimationPlayer.play("Turn")
	
	$WorldPattern/Pet.position = global_position + Vector2(-10, -10)
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func specific_process(delta):
	var pet_velocity = 100
	var desired_direction = (global_position - $WorldPattern/Pet.position).normalized() * pet_velocity * delta
	desired_direction = desired_direction.clamped($WorldPattern/Pet.position.distance_to(global_position) - 30)
	$WorldPattern/Pet.position = $WorldPattern/Pet.position + desired_direction
	
	pass


