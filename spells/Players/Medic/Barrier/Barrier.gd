extends "res://spells/Spell.gd"


var time_launch = 0
var tick_number = 0
var audio_played = false

# Called when the node enters the scene tree for the first time.
func _ready():
	time_launch = 6
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_launch = time_launch + delta
		
	
	
	if time_launch > (1.0 + tick_number) and time_launch < 6:
		if not audio_played:
			audio_manager.play_sfx(load("res://assets/audio/sfx/spaceEngine_000.ogg"), 2, 1)
			audio_played = true
		tick_number = tick_number + 0.5
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				#area.health += 50
				area.shield += damage
				compute_damage(get_parent().get_parent(), area, true)
		
		#var _new_jolt = $Lightning.instance()
		#add_child(_new_jolt)
		#_new_jolt.create(Vector2(100,1000), Vector2(300,300))
		#$Lightning.create(Vector2(0,0), Vector2(100,100))
		
		

	pass


func art_effect():
	$AnimationPlayer.play("attack")
	audio_played = false
	time_launch = 0
	tick_number = 0
	pass

func show_telegraph(value):
	if $AnimationPlayer.is_playing():
		pass
	else:
		$Sprite.visible = value
		
	pass
	
