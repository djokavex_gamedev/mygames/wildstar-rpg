extends "res://spells/Spell.gd"

var time_launch = 0
var tick_number = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	#$AnimationPlayer.play("attack")
	time_launch = 0
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_launch = time_launch + delta
		
	if time_launch > (0.2 + tick_number):
		if tick_number == 0:
			$CollisionShape2D.disabled = false
			$CollisionShapeStep2.disabled = true
			$CollisionShapeStep3.disabled = true
			
			var overlapping_areas = get_overlapping_bodies()
			for area in overlapping_areas:
				if area.is_in_group("enemy"):
					compute_damage(get_parent(), area, false)
		elif tick_number == 0.2:
			$CollisionShape2D.disabled = true
			$CollisionShapeStep2.disabled = false
			$CollisionShapeStep3.disabled = true
			
			var overlapping_areas = get_overlapping_bodies()
			for area in overlapping_areas:
				if area.is_in_group("enemy"):
					compute_damage(get_parent(), area, false)
		elif tick_number == 0.4:
			$CollisionShape2D.disabled = true
			$CollisionShapeStep2.disabled = true
			$CollisionShapeStep3.disabled = false
			
			var overlapping_areas = get_overlapping_bodies()
			for area in overlapping_areas:
				if area.is_in_group("enemy"):
					compute_damage(get_parent(), area, false)
		
		tick_number = tick_number + 0.2
		
		#if tick_number == 0.6:
		#	queue_free()
	pass

func art_effect():
	time_launch = 0
	tick_number = 0
	$AnimationPlayer.play("attack")
	pass

func play_sound():
	audio_manager.play_sfx(load("res://assets/audio/sfx/impactMetal_003.ogg"), 2, 1)
