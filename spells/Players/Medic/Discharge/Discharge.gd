extends "res://spells/Spell.gd"

var time_launch = 0
var tick_number = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	time_launch = 10
	pass # Replace with function body.
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_launch = time_launch + delta
		
	if time_launch > (tick_number) and time_launch < 2:
		tick_number = tick_number + 0.5
		audio_manager.play_sfx(load("res://assets/audio/sfx/doorOpen_001.ogg"), 2, 1)
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("enemy"):
				compute_damage(get_parent(), area, false)

	pass

func art_effect():
	$AnimationPlayer.play("attack")
	time_launch = 0
	tick_number = 0
	pass

func show_telegraph(value):
	if $AnimationPlayer.is_playing():
		pass
	else:
		$Sprite.visible = value
		
	pass
	
