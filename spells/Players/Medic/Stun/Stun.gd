extends "res://spells/Spell.gd"

func art_effect():
	#$AnimationPlayer.play("attack")
	#yield($AnimationPlayer, "animation_finished")
		
	var overlapping_areas = get_overlapping_bodies()
	for area in overlapping_areas:
		if area.is_in_group("enemy"):
			compute_damage(get_parent(), area, false)
			area.stun()
	pass

func show_telegraph(value):
	if $AnimationPlayer.is_playing():
		pass
	else:
		$Sprite.visible = value
		
	pass
	
