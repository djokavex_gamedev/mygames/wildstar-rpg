extends Node2D

var start_position = Vector2(0,0)
var end_position = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	global_position = start_position
	$AnimationPlayer.play("Fire")
	pass # Replace with function body.

func go_end_position():
	global_position = end_position
	pass

func play_explosion_sound():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Explosion_03.wav"), 1, 1)
	pass
