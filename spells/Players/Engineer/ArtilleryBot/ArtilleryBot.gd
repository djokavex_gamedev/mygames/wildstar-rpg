extends "res://spells/Spell.gd"

var missile = preload("res://spells/Players/Engineer/ArtilleryBot/Missile.tscn")

var audio_played = false

var target_position = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	$World/Bot.global_position = get_parent().get_parent().global_position - Vector2(10,10)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#target_position = get_parent().get_parent().global_position
	$Sprite.global_position = target_position
	$Particles2D.global_position = target_position
	$CollisionShape2D.global_position = target_position
	position = get_parent().get_parent().global_position
	
	if $AnimationPlayer.is_playing():
		pass
	else:
		var pet_velocity = 100
		var desired_direction = (get_parent().get_parent().global_position - $World/Bot.global_position).normalized() * pet_velocity * delta
		desired_direction = desired_direction.clamped($World/Bot.global_position.distance_to(get_parent().get_parent().global_position) - 30)
		$World/Bot.global_position = $World/Bot.global_position + desired_direction
	
	pass

func art_effect():
	$RayCast2D.force_raycast_update()
	if $RayCast2D.is_colliding():
		target_position = $RayCast2D.get_collision_point()
		#position = target_position
	else:
		target_position = global_position + Vector2($RayCast2D.cast_to.x,0).rotated(global_rotation)
		pass
	$AnimationPlayer.play("attack")
	audio_played = false
	pass

func show_telegraph(value):
	if $AnimationPlayer.is_playing():
		pass
	else:
		if value:
			$RayCast2D.force_raycast_update()
			#collision_particles.emitting = is_colliding()
			if $RayCast2D.is_colliding():
				target_position = $RayCast2D.get_collision_point()
			else:
				target_position = global_position + Vector2($RayCast2D.cast_to.x,0).rotated(global_rotation)
				pass
			
		$Sprite.visible = value
	pass

func check_collisions():
	var overlapping_areas = get_overlapping_bodies()
	for area in overlapping_areas:
		if area.is_in_group("enemy"):
			compute_damage(get_parent().get_parent(), area, false)

func spawn_missile():
	var miss = missile.instance()
	miss.start_position = $World/Bot.global_position
	miss.end_position = target_position
	$World.add_child(miss)
	pass
