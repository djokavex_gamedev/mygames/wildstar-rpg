extends "res://spells/Spell.gd"

func art_effect():
	$AnimationPlayer.play("attack")
	yield($AnimationPlayer, "animation_finished")
		
	audio_manager.play_sfx(load("res://assets/audio/sfx/laserLarge_000.ogg"), 2, 1)
	
	var overlapping_areas = get_overlapping_bodies()
	for area in overlapping_areas:
		if area.is_in_group("enemy"):
			compute_damage(get_parent(), area, false)
	pass

func show_telegraph(value):
	if $AnimationPlayer.is_playing():
		pass
	else:
		$Sprite.visible = value
		
	pass
	
