extends "res://spells/Spell.gd"

var missile = preload("res://spells/Players/Engineer/ArtilleryBot/Missile.tscn")

var audio_played = false

var target_position = Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	$World/Bot.global_position = get_parent().get_parent().global_position - Vector2(10,10)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#target_position = get_parent().get_parent().global_position
	#$Sprite.global_position = $World/Bot.global_position
	#$Particles2D.global_position = $World/Bot.global_position
	#$CollisionShape2D.global_position = $World/Bot.global_position
	position = $World/Bot.global_position
	
	if $AnimationPlayer.is_playing():
		pass
	else:
		var pet_velocity = 110
		var desired_direction = (get_parent().get_parent().global_position - $World/Bot.global_position).normalized() * pet_velocity * delta
		desired_direction = desired_direction.clamped($World/Bot.global_position.distance_to(get_parent().get_parent().global_position) - 30)
		$World/Bot.global_position = $World/Bot.global_position + desired_direction
	
	pass

func art_effect():
	target_position = $World/Bot.global_position
	$AnimationPlayer.play("attack")
	audio_played = false
	pass

func show_telegraph(value):
	if $AnimationPlayer.is_playing():
		pass
	else:
		if value:
			target_position = $World/Bot.global_position
			
		$Sprite.visible = value
	pass

func check_collisions():
	var overlapping_areas = get_overlapping_bodies()
	for area in overlapping_areas:
		if area.is_in_group("player"):
			compute_shield_gain(get_parent().get_parent(), area)

func spawn_missile():
	var miss = missile.instance()
	miss.start_position = $World/Bot.global_position
	miss.end_position = target_position
	$World.add_child(miss)
	pass
