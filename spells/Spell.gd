extends Area2D

export var static_spell = false
export var damage = 100
export var cost = 100
export var spell_gcd = 1.0

export var skill_power_mod = 0.35
export var level_stat_rate = 11.0

func use_art() -> bool:
	var parent = null
	if static_spell:
		parent = get_parent().get_parent()
	else:
		parent = get_parent()

	if $CD.is_stopped() and parent.focus > cost:
		#print("use spell")
		$CD.start()
		parent.focus = parent.focus - cost
		art_effect()
		return true
	else:
		return false
	pass

func art_effect():
	pass

func show_telegraph(value):
	$Sprite.visible = value
	pass
	
func _on_CD_timeout():
	pass # Replace with function body.

func get_CD():
	return $CD.get_time_left()
	pass

func compute_damage(caster, target, is_support):
	var is_crit = false
	if is_support:
		# Base heal for this spell
		var base_damage = caster.current_support_power * skill_power_mod + caster.current_level * level_stat_rate
		# Add some random
		var current_damage = rand_range(base_damage-base_damage*0.1, base_damage+base_damage*0.1)
		#Crit part
		randomize()
		if caster.current_crit_hit_chance > (randi() % 100):
			current_damage = current_damage + current_damage * caster.current_crit_hit_severity / 100.0
			is_crit = true
			pass
		
		target.set_health_with_crit(target.health+floor(current_damage), is_crit) 
		#target.health += floor(current_damage)
		
		randomize()
		if caster.current_multi_hit_chance > (randi() % 100):
			current_damage = current_damage * caster.current_multi_hit_severity / 100.0
			target.set_health_with_crit(target.health+floor(current_damage), is_crit) 
			#target.health += floor(current_damage)
			pass
		pass
	else:
		# Deflect attack
		randomize()
		if target.current_deflect_chance - caster.current_strikethrough > (randi() % 100):
			# Spell deflected
			#caster.health = caster.health
			target.deflected()
			return
			pass
		var bonus_pierce_from_strikethrough = 0.0
		if target.current_deflect_chance < caster.current_strikethrough:
			bonus_pierce_from_strikethrough = caster.current_strikethrough - target.current_deflect_chance
		
		# Base damage for this spell
		var base_damage = caster.current_assault_power * skill_power_mod + caster.current_level * level_stat_rate
		# Add some random
		var current_damage = rand_range(base_damage-base_damage*0.1, base_damage+base_damage*0.1)
		# Use target resistance and armor pierce
		current_damage = current_damage - current_damage * (target.current_physical_mitigation / 100.0) * (1.0 - (caster.current_armor_pierce+bonus_pierce_from_strikethrough) / 100.0)
		# Use vigor
		current_damage = current_damage + current_damage * (caster.health / caster.current_max_health) * caster.current_vigor / 100.0
		#Crit part
		randomize()
		if caster.current_crit_hit_chance > (randi() % 100):
			is_crit = true
			current_damage = current_damage + current_damage * caster.current_crit_hit_severity / 100.0
			pass
		
		# Apply damage
		target.set_health_with_crit(target.health-floor(current_damage), is_crit) 
		#target.health -= floor(current_damage)
		
		# Lifesteal
		if caster.current_lifesteal > 0:
			#caster.health += floor(caster.current_lifesteal * current_damage / 100.0)
			caster.set_health_with_crit(caster.health+floor(current_damage), is_crit) 
		
		# Multihit
		randomize()
		if caster.current_multi_hit_chance > (randi() % 100):
			current_damage = current_damage * caster.current_multi_hit_severity / 100.0
			target.set_health_with_crit(target.health-floor(current_damage), is_crit) 
			#target.health -= floor(current_damage)
			if caster.current_lifesteal > 0:
				caster.set_health_with_crit(caster.health+floor(current_damage), is_crit) 
				#caster.health += floor(caster.current_lifesteal * current_damage / 100.0)
			pass
	pass

func compute_shield_gain(caster, target):
	var is_crit = false
	# Base heal for this spell
	var base_damage = caster.current_support_power * skill_power_mod + caster.current_level * level_stat_rate
	# Add some random
	var current_damage = rand_range(base_damage-base_damage*0.1, base_damage+base_damage*0.1)
	#Crit part
	randomize()
	if caster.current_crit_hit_chance > (randi() % 100):
		is_crit = true
		current_damage = current_damage + current_damage * caster.current_crit_hit_severity / 100.0
		pass
	
	target.set_shield_with_crit(target.shield+floor(current_damage), is_crit)
	#target.shield += floor(current_damage)
	
	randomize()
	if caster.current_multi_hit_chance > (randi() % 100):
		current_damage = current_damage * caster.current_multi_hit_severity / 100.0
		target.set_shield_with_crit(target.shield+floor(current_damage), is_crit)
		#target.shield += floor(current_damage)
		pass
	pass
