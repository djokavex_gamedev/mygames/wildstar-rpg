extends "res://spells/Spell.gd"

var lightning_bolt = preload("res://effects/Lightning/Lightning.tscn")

var target_player

var time_launch = 0
var tick_number = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("attack")
	time_launch = 0
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_launch = time_launch + delta
	
	if time_launch < 1.0:
		if target_player != null:
			global_position = target_player.global_position
		pass
		
	if time_launch > (1.5 + tick_number):
		tick_number = tick_number + 0.5
		
		randomize()
		var light1 = lightning_bolt.instance()
		light1.spread_angle = 1.2
		add_child(light1)
		light1.create(global_position+Vector2(-24,rand_range(-15,15)), global_position+Vector2(24,rand_range(-15,15)))
		var light2 = lightning_bolt.instance()
		light2.spread_angle = 1.2
		add_child(light2)
		light2.create(global_position+Vector2(rand_range(-15,15),-24), global_position+Vector2(rand_range(-15,15),24))
		
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				compute_damage(get_parent().get_parent(), area, false)
		
		#var _new_jolt = $Lightning.instance()
		#add_child(_new_jolt)
		#_new_jolt.create(Vector2(100,1000), Vector2(300,300))
		#$Lightning.create(Vector2(0,0), Vector2(100,100))
		
		

	pass

func art_effect():
	
	pass
