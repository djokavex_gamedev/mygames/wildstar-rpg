#extends "res://spells/Spell.gd"
extends RigidBody2D

export var static_spell = false
export var damage = 100
export var cost = 100
export var spell_gcd = 1.0

export var skill_power_mod = 0.35
export var level_stat_rate = 11.0

var motion = Vector2(0.0,0.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("attack")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#translate(motion * delta)
	
	
	pass


func _on_DieTimer_timeout():
	queue_free()
	pass # Replace with function body.


func _on_BulletGravity_body_entered(body):
	#print("body enter")
	if body.is_in_group("player"):
		compute_damage(get_parent().get_parent(), body, false)
		motion = Vector2(0,0)
		#$CollisionShape2D.disabled = true
		$AnimationPlayer.play("destroy")
		collision_mask = 0
		yield($AnimationPlayer,"animation_finished")
		queue_free()
	else:
		motion = Vector2(0,0)
		#$CollisionShape2D.disabled = true
		$AnimationPlayer.play("destroy")
		collision_mask = 0
		yield($AnimationPlayer,"animation_finished")
		queue_free()
	pass # Replace with function body.

func compute_damage(caster, target, is_support):
	if is_support:
		# Base heal for this spell
		var base_damage = caster.current_support_power * skill_power_mod + caster.current_level * level_stat_rate
		# Add some random
		var current_damage = rand_range(base_damage-base_damage*0.1, base_damage+base_damage*0.1)
		#Crit part
		randomize()
		if caster.current_crit_hit_chance > (randi() % 100):
			current_damage = current_damage + current_damage * caster.current_crit_hit_severity / 100.0
			pass
		
		target.health += floor(current_damage)
		
		randomize()
		if caster.current_multi_hit_chance > (randi() % 100):
			current_damage = current_damage * caster.current_multi_hit_severity / 100.0
			target.health += floor(current_damage)
			pass
		pass
	else:
		# Deflect attack
		randomize()
		if target.current_deflect_chance - caster.current_strikethrough > (randi() % 100):
			# Spell deflected
			#caster.health = caster.health
			target.deflected()
			return
			pass
		var bonus_pierce_from_strikethrough = 0.0
		if target.current_deflect_chance < caster.current_strikethrough:
			bonus_pierce_from_strikethrough = caster.current_strikethrough - target.current_deflect_chance
		
		# Base damage for this spell
		var base_damage = caster.current_assault_power * skill_power_mod + caster.current_level * level_stat_rate
		# Add some random
		var current_damage = rand_range(base_damage-base_damage*0.1, base_damage+base_damage*0.1)
		# Use target resistance and armor pierce
		current_damage = current_damage - current_damage * (target.current_physical_mitigation / 100.0) * (1.0 - (caster.current_armor_pierce+bonus_pierce_from_strikethrough) / 100.0)
		# Use vigor
		current_damage = current_damage + current_damage * (caster.health / caster.current_max_health) * caster.current_vigor / 100.0
		#Crit part
		randomize()
		if caster.current_crit_hit_chance > (randi() % 100):
			current_damage = current_damage + current_damage * caster.current_crit_hit_severity / 100.0
			pass
		
		# Apply damage
		target.health -= floor(current_damage)
		# Lifesteal
		if caster.current_lifesteal > 0:
			caster.health += floor(caster.current_lifesteal * current_damage / 100.0)
		
		# Multihit
		randomize()
		if caster.current_multi_hit_chance > (randi() % 100):
			current_damage = current_damage * caster.current_multi_hit_severity / 100.0
			target.health -= floor(current_damage)
			if caster.current_lifesteal > 0:
				caster.health += floor(caster.current_lifesteal * current_damage / 100.0)
			pass
	pass
