extends "res://spells/Spell.gd"

var time_launch = 0
var tick_number = 0

var player = null

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("attack")
	time_launch = 0
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_launch = time_launch + delta
	
	
	
	if time_launch > (1.3 + tick_number):
		tick_number = tick_number + 1.3
		
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				compute_damage(get_parent().get_parent(), area, false)
	
	if time_launch > (0.5 + tick_number) and time_launch > 1.5:
		tick_number = tick_number + 30
		
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				compute_damage(get_parent().get_parent(), area, false)
	if time_launch < 1.5:
		if player != null:
			position = player.position
			get_parent().get_parent().attack_position = player.position 
		
	if time_launch > 3.1:
		queue_free()
	pass

func art_effect():
	
	pass


