extends "res://spells/Spell.gd"

var motion = Vector2(0.0,0.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("attack")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	translate(motion * delta)
	
	var overlapping_areas = get_overlapping_bodies()
	for area in overlapping_areas:
		if area.is_in_group("player"):
			area.health -= damage
			motion = Vector2(0,0)
			$CollisionShape2D.disabled = true
			$AnimationPlayer.play("destroy")
			yield($AnimationPlayer,"animation_finished")
			queue_free()
		else:
			motion = Vector2(0,0)
			$CollisionShape2D.disabled = true
			$AnimationPlayer.play("destroy")
			yield($AnimationPlayer,"animation_finished")
			queue_free()
	pass


func _on_DieTimer_timeout():
	queue_free()
	pass # Replace with function body.
