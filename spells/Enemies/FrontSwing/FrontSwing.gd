extends "res://spells/Spell.gd"

var time_launch = 0
var tick_number = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	#$AnimationPlayer.play("attack")
	time_launch = 0
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_launch = time_launch + delta
		
	if time_launch > (0.5 + tick_number):
		tick_number = tick_number + 0.5
		
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				compute_damage(get_parent(), area, false)
				
		queue_free()
	pass

func art_effect():
	
	pass
