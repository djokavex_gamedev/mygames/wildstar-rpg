extends "res://spells/Spell.gd"

var lightning_bolt = preload("res://effects/Lightning/Lightning.tscn")

var enable_rotation = false
var rotation_side = 1
var rotation_speed = 0.5

var time_launch = 0
var tick_number = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("attack")
	time_launch = 0
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_launch = time_launch + delta
	#print("" + str(time_launch) + " " + str(tick_number))
	if time_launch > (1.0 + tick_number):
		tick_number = tick_number + 0.2
		
		var light1 = lightning_bolt.instance()
		add_child(light1)
		var current_rotation = Vector2(300,0).rotated(rotation+(rotation_speed/10.0)*rotation_side)
		light1.create(global_position+Vector2(0,0), global_position+current_rotation)
		var light2 = lightning_bolt.instance()
		add_child(light2)
		current_rotation = Vector2(0,300).rotated(rotation+(rotation_speed/10.0)*rotation_side)
		light2.create(global_position+Vector2(0,0), global_position+current_rotation)
		var light3 = lightning_bolt.instance()
		add_child(light3)
		current_rotation = Vector2(-300,0).rotated(rotation+(rotation_speed/10.0)*rotation_side)
		light3.create(global_position+Vector2(0,0), global_position+current_rotation)
		var light4 = lightning_bolt.instance()
		add_child(light4)
		current_rotation = Vector2(0,-300).rotated(rotation+(rotation_speed/10.0)*rotation_side)
		light4.create(global_position+Vector2(0,0), global_position+current_rotation)
		
		audio_manager.play_sfx(load("res://assets/audio/thunder.wav"), 1, 4)
		
		var overlapping_areas = get_overlapping_bodies()
		for area in overlapping_areas:
			if area.is_in_group("player"):
				compute_damage(get_parent().get_parent(), area, false)
		
	if time_launch > 1.0:
		if enable_rotation:
			global_rotation = global_rotation + rotation_speed * delta * rotation_side
		
		#var _new_jolt = $Lightning.instance()
		#add_child(_new_jolt)
		#_new_jolt.create(Vector2(100,1000), Vector2(300,300))
		#$Lightning.create(Vector2(0,0), Vector2(100,100))
		
		

	pass

func art_effect():
	
	pass
