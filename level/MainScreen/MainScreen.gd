extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	# Run music
	audio_manager.play_music(load("res://assets/audio/music/001. WildStar.ogg"))
	$KeyBindMenu.load_keys()
	$AudioMenu.load_volume()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_NewSoloGame_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	get_tree().change_scene("res://level/TestLevel/TestLevel.tscn")
	pass # Replace with function body.


func _on_NewOnlineGame_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	pass # Replace with function body.


func _on_Options_pressed():
	#$KeyBindMenu.load_keys()
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	$OptionMainMenu.visible = true
	pass # Replace with function body.


func _on_Exit_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	get_tree().quit()
	pass # Replace with function body.



