extends ColorRect



func _on_AudioButton_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	visible = false
	get_parent().get_node("AudioMenu").visible = true
	pass # Replace with function body.


func _on_KeybindButton_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	visible = false
	get_parent().get_node("KeyBindMenu").visible = true
	pass # Replace with function body.


func _on_ExitButton_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	visible = false
	pass # Replace with function body.
