extends CanvasLayer


# Called when the node enters the scene tree for the first time.
func _ready():
	audio_manager.play_music(load("res://assets/audio/music/001. WildStar.ogg"))
	EventBus.connect("player_level_up", self, "on_level_up")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if get_parent().get_node_or_null("MyPlayer") != null:
		$LifeBar.value = get_parent().get_node("MyPlayer").health
		$LifeBar.max_value = get_parent().get_node("MyPlayer").base_max_health
		$ShieldBar.value = get_parent().get_node("MyPlayer").shield
		$ShieldBar.max_value = get_parent().get_node("MyPlayer").base_max_shield
		$DodgeBar.value = get_parent().get_node("MyPlayer").dodge_pool
		
		if get_parent().get_node("MyPlayer").action1_spell != null:
			$Action1.texture = get_parent().get_node("MyPlayer").action1_spell.get_node("SpellImage").texture
			var time_cd = max(get_parent().get_node("MyPlayer").get_GCD_value(), get_parent().get_node("MyPlayer").action1_spell.get_CD())
			if floor((time_cd) * 10.0) / 10.0 < 0.1:
				$Action1/Label.visible = false
			else:
				$Action1/Label.visible = true
			$Action1/Label.text = str( floor((time_cd) * 10.0) / 10.0 )
			if get_parent().get_node("MyPlayer").action1_spell.cost < get_parent().get_node("MyPlayer").focus:
				$Action1/Shadow.visible = false
			else:
				$Action1/Shadow.visible = true
		else:
			$Action1.visible = false
			
		if get_parent().get_node("MyPlayer").action2_spell != null:
			$Action2.texture = get_parent().get_node("MyPlayer").action2_spell.get_node("SpellImage").texture
			var time_cd = max(get_parent().get_node("MyPlayer").get_GCD_value(), get_parent().get_node("MyPlayer").action2_spell.get_CD())
			if floor((time_cd) * 10.0) / 10.0 < 0.1:
				$Action2/Label.visible = false
			else:
				$Action2/Label.visible = true
			$Action2/Label.text = str( floor((time_cd) * 10.0) / 10.0 )
			if get_parent().get_node("MyPlayer").action2_spell.cost < get_parent().get_node("MyPlayer").focus:
				$Action2/Shadow.visible = false
			else:
				$Action2/Shadow.visible = true
		else:
			$Action2.visible = false
			
		if get_parent().get_node("MyPlayer").action3_spell != null:
			$Action3.texture = get_parent().get_node("MyPlayer").action3_spell.get_node("SpellImage").texture
			var time_cd = max(get_parent().get_node("MyPlayer").get_GCD_value(), get_parent().get_node("MyPlayer").action3_spell.get_CD())
			if floor((time_cd) * 10.0) / 10.0 < 0.1:
				$Action3/Label.visible = false
			else:
				$Action3/Label.visible = true
			$Action3/Label.text = str( floor((time_cd) * 10.0) / 10.0 )
			if get_parent().get_node("MyPlayer").action3_spell.cost < get_parent().get_node("MyPlayer").focus:
				$Action3/Shadow.visible = false
			else:
				$Action3/Shadow.visible = true
		else:
			$Action3.visible = false
			
		if get_parent().get_node("MyPlayer").action4_spell != null:
			$Action4.texture = get_parent().get_node("MyPlayer").action4_spell.get_node("SpellImage").texture
			var time_cd = max(get_parent().get_node("MyPlayer").get_GCD_value(), get_parent().get_node("MyPlayer").action4_spell.get_CD())
			if floor((time_cd) * 10.0) / 10.0 < 0.1:
				$Action4/Label.visible = false
			else:
				$Action4/Label.visible = true
			$Action4/Label.text = str( floor((time_cd) * 10.0) / 10.0 )
			if get_parent().get_node("MyPlayer").action4_spell.cost < get_parent().get_node("MyPlayer").focus:
				$Action4/Shadow.visible = false
			else:
				$Action4/Shadow.visible = true
		else:
			$Action4.visible = false
			
		if get_parent().get_node("MyPlayer").stun_ability != null:
			$Action5.texture = get_parent().get_node("MyPlayer").stun_ability.get_node("SpellImage").texture
			var time_cd = max(get_parent().get_node("MyPlayer").get_GCD_value(), get_parent().get_node("MyPlayer").stun_ability.get_CD())
			if floor((time_cd) * 10.0) / 10.0 < 0.1:
				$Action5/Label.visible = false
			else:
				$Action5/Label.visible = true
			$Action5/Label.text = str( floor((time_cd) * 10.0) / 10.0 )
			if get_parent().get_node("MyPlayer").stun_ability.cost < get_parent().get_node("MyPlayer").focus:
				$Action5/Shadow.visible = false
			else:
				$Action5/Shadow.visible = true
		else:
			$Action5.visible = false
		
	pass

func on_level_up():
	$LevelUP.emitting = true
	pass
