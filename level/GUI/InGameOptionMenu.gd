extends CanvasLayer


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_AudioButton_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	$ColorRect/AudioMenu.visible = true
	pass # Replace with function body.


func _on_KeyButton_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	$ColorRect/KeyBindMenu.visible = true
	pass # Replace with function body.


func _on_SaveButton_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	get_tree().change_scene("res://level/MainScreen/MainScreen.tscn")
	pass # Replace with function body.


func _on_ReturnButton_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	$ColorRect.visible = false
	pass # Replace with function body.
