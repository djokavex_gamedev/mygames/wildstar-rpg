extends ColorRect


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func load_keys():
	print("Loading keys")
	
	var data = PersistenceNode.get_data("keybind")
	#print(data)
	
	var new_input_key = InputEventKey.new()
	var new_input_mouse = InputEventMouseButton.new()
	var new_input_pad = InputEventJoypadButton.new()
	
	var current_button = $Panel/VBoxContainer/Up/ActionRemapButton
	var current_action = $Panel/VBoxContainer/Up/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
			
	current_button = $Panel/VBoxContainer/Down/ActionRemapButton
	current_action = $Panel/VBoxContainer/Down/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Left/ActionRemapButton
	current_action = $Panel/VBoxContainer/Left/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Right/ActionRemapButton
	current_action = $Panel/VBoxContainer/Right/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Action1/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action1/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Action2/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action2/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Action3/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action3/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Action4/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action4/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Counter/ActionRemapButton
	current_action = $Panel/VBoxContainer/Counter/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	current_button = $Panel/VBoxContainer/Dodge/ActionRemapButton
	current_action = $Panel/VBoxContainer/Dodge/ActionRemapButton.action
	if data.has(current_action):
		InputMap.action_erase_events(current_action)
		if data[current_action]["Type"] == "InputEventKey":
			new_input_key.set_scancode(OS.find_scancode_from_string(data[current_action]["Data"]))
			current_button.remap_action_to(new_input_key)
			new_input_key = InputEventKey.new()
		if data[current_action]["Type"] == "InputEventMouseButton":
			new_input_mouse.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_mouse)
			new_input_mouse = InputEventMouseButton.new()
		if data[current_action]["Type"] == "InputEventJoypadButton":
			new_input_pad.button_index = int(data[current_action]["Data"])
			current_button.remap_action_to(new_input_pad)
			new_input_pad = InputEventJoypadButton.new()
	
	
	
	pass

func save_keybind():
	print("Saving keys")

	var data = PersistenceNode.get_data("keybind")
	#print(data)

	var current_button = $Panel/VBoxContainer/Up/ActionRemapButton
	var current_action = $Panel/VBoxContainer/Up/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Down/ActionRemapButton
	current_action = $Panel/VBoxContainer/Down/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Left/ActionRemapButton
	current_action = $Panel/VBoxContainer/Left/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Right/ActionRemapButton
	current_action = $Panel/VBoxContainer/Right/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Action1/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action1/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Action2/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action2/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Action3/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action3/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Action4/ActionRemapButton
	current_action = $Panel/VBoxContainer/Action4/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Counter/ActionRemapButton
	current_action = $Panel/VBoxContainer/Counter/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	current_button = $Panel/VBoxContainer/Dodge/ActionRemapButton
	current_action = $Panel/VBoxContainer/Dodge/ActionRemapButton.action
	data[current_action] = {
		"Type" : current_button.display_current_key_type(),
		"Data" : current_button.display_current_key()
	}
	
	PersistenceNode.save_data("keybind")
	
	Notifications.notify("Keybinds saved!")
	
	pass

func _on_Save_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	save_keybind()
	visible = false
	pass # Replace with function body.


func _on_Back_pressed():
	audio_manager.play_sfx(load("res://assets/audio/sfx/Menu_Navigate_00.wav"), 1, 1)
	visible = false
	pass # Replace with function body.
