extends TextureRect

var itemName;
var itemValue;
var itemSlot;
var slotType;
var picked = false;
var rarity = 0;

var stats = []

var rng = RandomNumberGenerator.new()

func _init(_itemName, _itemTexture, _itemSlot, _itemValue, _slotType):
	rng.randomize();
	self.itemName = _itemName;
	self.itemValue = _itemValue;
	self.itemSlot = _itemSlot;
	self.slotType = _slotType;
	self.rarity = rng.randi_range(global.ItemRarity.NORMAL, global.ItemRarity.LEGENDARY);
	texture = _itemTexture;
	expand = true
	rect_size = Vector2(30,30)
	rect_min_size = Vector2(30,30)
	mouse_filter = Control.MOUSE_FILTER_PASS;
	mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND;
	
	
	#test
	match self.rarity:
		global.ItemRarity.NORMAL:
			var current_stat = rng.randi_range(0, global.STATS_MAX-1)
			var current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade,current_random_stat_upgrade*2)])
		global.ItemRarity.MAGIC:
			var current_stat = rng.randi_range(0, global.STATS_MAX-1)
			var current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
			current_stat = rng.randi_range(0, global.STATS_MAX-1)
			current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
		global.ItemRarity.RARE:
			var current_stat = rng.randi_range(0, global.STATS_MAX-1)
			var current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
			current_stat = rng.randi_range(0, global.STATS_MAX-1)
			current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
			current_stat = rng.randi_range(0, global.STATS_MAX-1)
			current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
		global.ItemRarity.LEGENDARY:
			var current_stat = rng.randi_range(0, global.STATS_MAX-1)
			var current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
			current_stat = rng.randi_range(0, global.STATS_MAX-1)
			current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
			current_stat = rng.randi_range(0, global.STATS_MAX-1)
			current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
			current_stat = rng.randi_range(0, global.STATS_MAX-1)
			current_random_stat_upgrade = global.STATS_ON_STUFF[current_stat]
			stats.append([current_stat, rng.randi_range(current_random_stat_upgrade*global.RarityBoost[global.ItemRarity.MAGIC],current_random_stat_upgrade*2*global.RarityBoost[global.ItemRarity.MAGIC])])
			
			
func pickItem():
	mouse_filter = Control.MOUSE_FILTER_IGNORE;
	picked = true;

func putItem():
	rect_position = Vector2(0, 0);
	mouse_filter = Control.MOUSE_FILTER_PASS;
	picked = false;
