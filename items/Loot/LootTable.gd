extends Node

var data = []
var output_data = []

#adds item to database
func add_item(item, percent):
	data.append([item, percent])

#removes item from database
func remove_item(value):
	data.erase(value)

func create_from_array(array):
	data = array

func roll(max_items):
	for i in data.size():
		if output_data.size() < max_items: 
			randomize()
			var rand_value = randi() % 100
			#print(data[i][0] + " " + str(data[i][1]) + " " + str(rand_value))
			if data[i][1] > (rand_value):
				output_data.append(data[i][0])
	pass
